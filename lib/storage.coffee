class Storage
    PREFIX: 'st'
    constructor: (queue) ->
        @_keys =
            queue: "#{@PREFIX}:queue:#{queue}"
            data: "#{@PREFIX}:data:#{queue}"
    get: (id, cb) ->
    mget: (ids, cb) ->
    set: (id, data, cb) ->
    remove: (id, cb) ->
    enqueue: (id, time, cb) ->
    requeue: (id, time, cb) ->
    dequeue: (id, cb) ->
    queued: (upTo, cb) ->

module.exports = Storage
