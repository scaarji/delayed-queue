class Task
    constructor: (data) ->
        @_data = {}
        for own k, v of data
            if typeof data isnt 'function' and k isnt '_lastRanAt'
                @_data[k] = v

        @id = @_data.id
        @lastRanAt = data._lastRanAt or null

    getTask: -> @_data
    getId: -> @id

    toObject: ->
        data = {}
        data[k] = v for own k, v of @_data
        data._lastRanAt = @lastRanAt
        data

    eql: (data) ->
        keys = (k for own k of data)
        keys.push k for own k of @_data when k not in keys

        for k in keys
            unless data[k] and @_data[k] and @_data[k] is data[k]
                return no
        yes

    merge: (data) ->
        _hash = {}
        keys =
            for own k of data
                _hash[k] = yes
                k
        for own k of @_data when _hash[k] isnt yes
            keys.push k

        @_data[k] = data[k] or @_data[k] for k in keys

module.exports = Task
