async = require 'async'

Task = require './task'

class Queue
    # interval of task queueing
    updateInterval: 5000
    log: console.log
    constructor: (@name, actions, interval, @storage) ->
        @_run = @_run.bind(this)
        @_runTask = @_runTask.bind(this)
        @add = @add.bind(this)
        @interval interval

        if typeof actions is 'function'
            @_actions =
                process: actions
                test: (item, cb) -> cb null, yes
        else
            @_actions = actions
            unless @_actions.test? and typeof @_actions.test is 'function'
                @_actions.test = (item, cb) -> cb null, yes

    interval: (value = null) ->
        if value?
            if typeof value is 'function'
                @_interval = value
            else
                @_interval = -> value
        @_interval

    add: (item, cb) ->
        task = new Task item

        async.waterfall [
            (cb) =>
                @get task.getId(), cb
            (item, cb) =>
                if item
                    item.merge task.toObject()
                    task = item

                async.waterfall [
                    (cb) =>
                        @save task, cb
                    (task, cb) =>
                        if item
                            @bump task, cb
                        else
                            @storage.enqueue task.getId(), 1, cb
                ], cb
        ], cb

    save: (item, cb) ->
        task = if item instanceof Task then item else new Task item
        @storage.set task.getId(), task.toObject(), (err) ->
            cb err, task

    get: (id, cb) ->
        async.waterfall [
            (cb) =>
                @storage.get id, cb
            (item, cb) ->
                if item
                    cb null, new Task item
                else
                    cb null, null
        ], cb

    remove: (id, cb) ->
        async.series [
            (cb) => @storage.dequeue id, cb
            (cb) => @storage.remove id, cb
        ], cb

    bump: (item, cb) ->
        task = if item instanceof Task then item else new Task item
        interval = @_interval(task.toObject())
        if interval is no
            @remove task.getId(), cb
        else
            @storage.requeue task.getId(),
                (task.lastRanAt or Date.now()) + interval, cb

    _runTask: (item, cb) ->
        task = if item instanceof Task then item else new Task item
        async.waterfall [
            (cb) =>
                @bump task, (err) -> cb err
            (cb) =>
                unless @_actions.test? and typeof @_actions.test is 'function'
                    return cb null, yes

                @_actions.test.call @, task.getTask(), cb
            (run, cb) =>
                return cb null unless run

                if @_actions.process.length is 2
                    @_actions.process.call @, task.getTask(), (err) -> cb err
                else
                    @_actions.process.call @, task.getTask()
                    cb null
            (cb) =>
                task.lastRanAt = Date.now()
                async.parallel [
                    (cb) => @bump task, cb
                    (cb) => @save task, cb
                ], cb
        ], (err) =>
            if err
                @remove task.getId(), (err) =>
                    if err
                        @log err.message,
                            stack: err.stack
                            task: task.toObject()
            cb err

    _runTasks: (items, cb) ->
        for own id, item of items
            do (item) =>
                if item
                    task = new Task(item)
                    @_runTask task, (err) =>
                        if err
                            @log err.message,
                                stack: err.stack
                                task: task.toObject()
                else
                    @remove id, (err) =>
                        if err
                            @log err.message,
                                stack: err.stack
                                task: id
        cb null

    _run: ->
        async.waterfall [
            (cb) =>
                @storage.queued Date.now(), cb
            (ids, cb) =>
                @storage.mget ids, cb
            (items, cb) =>
                @_runTasks items, cb
        ], (err) =>
            @log err.message, stack: err.stack if err

    stop: ->
        clearInterval @_iid if @_iid
    start: ->
        @_run()
        @_iid = setInterval @_run, @updateInterval

module.exports = Queue
