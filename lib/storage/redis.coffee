async = require 'async'

Storage = require '../storage'

class RedisStorage extends Storage
    constructor: (queue, @redis) ->
        super(queue)
    get: (id, cb) ->
        return cb new Error "Id is not given" unless id
        async.waterfall [
            (cb) =>
                @redis.hget @_keys.data, id, cb
            (data, cb) ->
                return cb null, null unless data
                try
                    data = JSON.parse data
                catch err
                    return cb err

                cb null, data
        ], cb
    mget: (ids, cb) ->
        return cb new Error 'Ids not given' unless ids
        unless ids instanceof Array
            return cb new Error 'Ids should be instance of array'
        return cb null, [] unless ids.length
        async.waterfall [
            (cb) =>
                @redis.hmget @_keys.data, ids, cb
            (items, cb) ->
                return cb null, null unless items
                result = {}
                try
                    result[id] = JSON.parse items[i] for id, i in ids
                catch err
                    cb err
                cb null, result
        ], cb
    set: (id, data, cb) ->
        return cb new Error "Empty id or data" unless id and data
        @redis.hset @_keys.data, id, JSON.stringify(data), (err) -> cb err
    remove: (id, cb) ->
        return cb new Error 'Id is not given' unless id
        @redis.hdel @_keys.data, id, (err) -> cb err
    clear: (cb) ->
        @redis
            .multi()
            .del(@_keys.queue)
            .del(@_keys.data)
            .exec (err) -> cb err
    enqueue: (id, time, cb) ->
        unless id and time
            return cb new Error 'Id or time is not given'
        unless typeof time is 'number'
            return cb new Error 'Time should be number'
        @redis.zadd @_keys.queue, time, id, (err) -> cb err
    dequeue: (id, cb) ->
        return cb new Error 'Id is not given'unless id
        @redis.zrem @_keys.queue, id, (err) -> cb err
    requeue: (id, time, cb) ->
        unless id and time
            return cb new Error 'Id or time is not given'
        unless typeof time is 'number'
            return cb new Error 'Time should be number'
        async.waterfall [
            (cb) =>
                @redis.zscore @_keys.queue, id, cb
            (currentTime, cb) =>
                if currentTime?
                    currentTime = parseInt(currentTime, 10)
                    return cb null if currentTime and currentTime < time
                @redis.zadd @_keys.queue, time, id, (err) -> cb err
        ], cb
    queued: (upTo, cb) ->
        async.waterfall [
            (cb) =>
                @redis.zrangebyscore @_keys.queue, 1, upTo, cb
            (ids, cb) =>
                multi = @redis.multi()
                multi.zadd @_keys.queue, 0, id for id in ids
                multi.exec (err) ->
                    cb err, ids
        ], cb

module.exports = RedisStorage
