async = require 'async'

Storage = require '../storage'

class MemoryStorage extends Storage
    constructor: ->
        @_items = {}
        @_queue = []

    get: (id, cb) ->
        return cb new Error "Id is not given" unless id
        cb null, @_items[id]

    mget: (ids, cb) ->
        return cb new Error "Ids not given" unless ids
        unless ids instanceof Array
            return cb new Error 'Ids should be instance of array'

        result = {}
        for id in ids
            result[id] = @_items[id]
        cb null, result

    set: (id, data, cb) ->
        return cb new Error "Empty id or data" unless id and data
        @_items[id] = data
        cb null

    remove: (id, cb) ->
        return cb new Error 'Id is not given' unless id
        @_items[id] = null
        cb null

    clear: (cb) ->
        @_items = {}
        @_queue = []

    enqueue: (id, time, cb) ->
        unless id and time
            return cb new Error 'Id or time is not given'
        unless typeof time is 'number'
            return cb new Error 'Time should be number'

        for item, i in @_queue when item.time > time
            @_queue.splice i, 0, id: id, time: time
            return cb null

        @_queue.push id: id, time: time
        cb null

    dequeue: (id, cb) ->
        return cb new Error 'Id is not given' unless id
        for item, i in @_queue when item.id is id
            @_queue.splice i, 1
            break
        cb null

    requeue: (id, time, cb) ->
        unless id and time
            return cb new Error 'Id or time is not given'
        unless typeof time is 'number'
            return cb new Error 'Time should be number'

        for item, i in @_queue when item.id is id
            currentTime = parseInt(item.time, 10)
            unless currentTime and currentTime < time
                @_queue[i].time = time

            return cb null

        @enqueue id, time, cb

    queued: (upTo, cb) ->
        ids = []
        for item, i in @_queue when item.time > 0
            break if item.time > upTo

            @_queue[i].time = 0
            ids.push item.id

        cb null, ids

module.exports = MemoryStorage
