should = require 'should'
sinon = require 'sinon'
async = require 'async'

Queue = require '../lib/queue'
Task = require '../lib/task'
Storage = require '../lib/storage'

describe 'Queue', ->
    queue = null
    storage = null
    beforeEach ->
        storage = new Storage
        queue = new Queue 'test',
            process: (item, cb) -> cb null
        , 10, storage

    describe '#interval', ->
        it 'should return current interval function if value is not given', ->
            queue.interval().should.be.type 'function'
        it 'should save given function if function is given', ->
            interval = -> 10
            queue.interval interval
            queue._interval.should.eql interval
        it 'should create function from non function value', ->
            queue.interval 10
            queue._interval.should.be.type 'function'
            queue._interval().should.eql 10

    describe '#add', ->
        it 'should add task if it does not exist', (done) ->
            tasks = [
                {
                    input:
                        id: 'id'
                    output:
                        id: 'id'
                        _lastRanAt: null
                }
                {
                    input:
                        id: 'other id'
                        foo: 'bar'
                        bar: 'baz'
                    output:
                        id: 'other id'
                        foo: 'bar'
                        bar: 'baz'
                        _lastRanAt: null
                }
            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.get = (id, cb) -> cb null, null
                queue.storage.set = (id, data, cb) -> cb null
                queue.storage.enqueue = (id, time, cb) -> cb null

                getSpy = sinon.spy queue.storage, 'get'
                setSpy = sinon.spy queue.storage, 'set'
                enqueueSpy = sinon.spy queue.storage, 'enqueue'

                async.series [
                    (cb) ->
                        queue.add task.input, cb
                    (cb) ->
                        getSpy.calledOnce.should.eql yes
                        setSpy.calledOnce.should.eql yes
                        enqueueSpy.calledOnce.should.eql yes

                        getSpy.args[0][0].should.eql task.input.id

                        setSpy.args[0][0].should.eql task.input.id
                        setSpy.args[0][1].should.eql task.output

                        enqueueSpy.args[0][0].should.eql task.input.id
                        enqueueSpy.args[0][1].should.eql 1

                        cb null
                ], cb
            , done

        it 'should update and requeue existing task if it exists', (done) ->
            tasks = [
                {
                    originalTask:
                        id: 'id'
                    updatedTask:
                        id: 'id'
                        data: 'data'
                    resultingTask:
                        id: 'id'
                        data: 'data'
                        _lastRanAt: null
                    originalInterval: Date.now() + 1000
                    updatedInterval: Date.now() + 10
                }
                {
                    originalTask:
                        id: 'id2'
                        data: 'dat'
                        data2: 'dat2'
                        data3: 'dat3'
                    updatedTask:
                        id: 'id2'
                        data: 'data'
                        data2: 'data2'
                    resultingTask:
                        id: 'id2'
                        data: 'data'
                        data2: 'data2'
                        data3: 'dat3'
                        _lastRanAt: null
                    originalInterval: 2
                    updatedInterval: Date.now() + 2
                }
            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.get = (id, cb) -> cb null, task.originalTask
                queue.storage.set = (id, data, cb) -> cb null
                queue.storage.requeue = (id, time, cb) -> cb null

                getSpy = sinon.spy queue.storage, 'get'
                setSpy = sinon.spy queue.storage, 'set'
                requeueSpy = sinon.spy queue.storage, 'requeue'

                async.series [
                    (cb) ->
                        queue.interval 10
                        queue.add task.updatedTask, cb
                    (cb) ->
                        getSpy.calledOnce.should.eql yes
                        setSpy.calledOnce.should.eql yes
                        requeueSpy.calledOnce.should.eql yes

                        getSpy.args[0][0].should.eql task.updatedTask.id

                        setSpy.args[0][0].should.eql task.updatedTask.id
                        setSpy.args[0][1].should.eql task.resultingTask

                        requeueSpy.args[0][0].should.eql task.updatedTask.id
                        requeueSpy.args[0][1].should.be.approximately(
                            task.updatedInterval, 20)
                        cb null
                ], cb
            , done

    describe '#save', ->
        it 'should save task data', (done) ->
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                    _lastRanAt: null
                }
                {
                    id: 'id2'
                    data2: 'data2'
                    _lastRanAt: null
                }

            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.set = (id, data, cb) -> cb null
                setSpy = sinon.spy queue.storage, 'set'
                async.series [
                    (cb) ->
                        queue.save task, cb
                    (cb) ->
                        setSpy.calledOnce.should.eql yes

                        setSpy.args[0][0].should.eql task.id
                        setSpy.args[0][1].should.eql task

                        cb null
                ], cb
            , done

    describe '#get', ->
        it 'should retrieve task data by id', (done) ->
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                }
                {
                    id: 'id2'
                    data2: 'data2'
                }

            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.get = (id, cb) -> cb null, task
                getSpy = sinon.spy queue.storage, 'get'
                async.waterfall [
                    (cb) ->
                        storage.get task.id, cb
                    (storedValue, cb) ->
                        storedValue.should.eql task
                        getSpy.calledOnce.should.eql yes

                        getSpy.args[0][0].should.eql task.id

                        cb null
                ], cb
            , done

    describe '#remove', ->
        it 'should remove item data', (done) ->
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                }
                {
                    id: 'id2'
                    data2: 'data2'
                }

            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.remove = (id, cb) -> cb null
                queue.storage.dequeue = (id, cb) -> cb null
                removeSpy = sinon.spy queue.storage, 'remove'
                dequeueSpy = sinon.spy queue.storage, 'dequeue'
                async.series [
                    (cb) ->
                        queue.remove task.id, cb
                    (cb) ->
                        removeSpy.calledOnce.should.eql yes
                        dequeueSpy.calledOnce.should.eql yes

                        removeSpy.args[0][0].should.eql task.id
                        dequeueSpy.args[0][0].should.eql task.id

                        cb null
                ], cb
            , done

    describe '#bump', ->
        it 'should remove item if interval returns false for given item', (done) ->
            queue.interval no
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                }
                {
                    id: 'id2'
                    data: 'data2'
                }
            ]
            queue.storage.remove = (id, cb) -> cb null
            queue.storage.dequeue = (id, cb) -> cb null

            spy = sinon.spy queue, 'remove'
            async.eachSeries tasks, (task, cb) ->
                async.series [
                    (cb) ->
                        queue.bump task, cb
                    (cb) ->
                        spy.getCall(spy.callCount - 1).args[0].should.eql task.id

                        cb null
                ], cb
            , done

        it 'should enqueue item if interval returns numeric value', (done) ->
            interval = 10000
            queue.interval interval
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                    _lastRanAt: 10000
                }
                {
                    id: 'id2'
                    data: 'data2'
                }
            ]
            async.eachSeries tasks, (task, cb) ->
                queue.storage.requeue = (id, time, cb) -> cb null
                requeueSpy = sinon.spy queue.storage, 'requeue'

                async.series [
                    (cb) ->
                        queue.bump task, cb
                    (cb) ->
                        requeueSpy.calledOnce.should.eql yes
                        requeueSpy.args[0][0].should.eql task.id
                        offset = task._lastRanAt or Date.now()
                        requeueSpy.args[0][1]
                            .should.be.approximately offset + interval, 20

                        cb null
                ], cb
            , done

    describe '#_runTask', ->
        it 'should not process task if test fails', (done) ->
            queue._actions.test = (item, cb) -> cb null, no
            testSpy = sinon.spy queue._actions, 'test'
            processSpy = sinon.spy queue._actions, 'process'

            queue.storage.requeue = (id, time, cb) -> cb null
            queue.storage.set = (id, data, cb) -> cb null

            queue._runTask id: 'id', (err) ->
                should.not.exist err
                testSpy.calledOnce.should.eql yes
                processSpy.called.should.eql no
                done null

        it 'should remove task if error occures', (done) ->
            queue.storage.requeue = (id, time, cb) -> cb null
            queue.storage.set = (id, data, cb) -> cb null
            queue.storage.remove = (id, cb) -> cb null

            removeSpy = sinon.spy queue, 'remove'

            async.series [
                (cb) ->
                    queue._actions.test = (item, cb) ->
                        cb new Error 'fake test error'
                    queue._runTask id: 'id', (err) ->
                        should.exist err
                        err.message.should.eql 'fake test error'

                        removeSpy.calledOnce.should.eql yes
                        cb null
                (cb) ->
                    queue._actions.test = (item, cb) -> cb null, yes
                    queue._actions.process = (item, cb) ->
                        cb new Error 'fake process error'
                    queue._runTask id: 'id', (err) ->
                        should.exist err
                        err.message.should.eql 'fake process error'
                        removeSpy.calledTwice.should.eql yes
                        cb null
            ], done

        it 'should run normally if no error occurs', (done) ->
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                }
                {
                    id: 'id2'
                    data: 'data2'
                }
            ]
            queue._actions.test = (item, cb) -> cb null, yes
            queue._actions.process = (item, cb) -> cb null
            testSpy = sinon.spy queue._actions, 'test'
            processSpy = sinon.spy queue._actions, 'process'

            saveSpy = sinon.spy queue, 'save'
            bumpSpy = sinon.spy queue, 'bump'

            queue.storage.requeue = (id, time, cb) -> cb null
            queue.storage.dequeue = (id, cb) -> cb null
            queue.storage.remove = (id, cb) -> cb null
            queue.storage.set = (id, data, cb) -> cb null

            async.eachSeries tasks, (task, cb) ->
                queue._runTask task, cb
            , (err) ->
                testSpy.calledTwice.should.eql yes
                processSpy.calledTwice.should.eql yes
                saveSpy.calledTwice.should.eql yes
                # 2 per run
                bumpSpy.callCount.should.eql 4
                done err

    describe '#_run', ->
        it 'should get queued tasks and run them', (done) ->
            tasks = [
                {
                    id: 'id1'
                    data: 'data1'
                }
                {
                    id: 'id2'
                    data: 'data2'
                }
            ]
            queue._actions.test = (item, cb) -> cb null, yes
            queue._actions.process = (item, cb) -> cb null
            testSpy = sinon.spy queue._actions, 'test'
            processSpy = sinon.spy queue._actions, 'process'

            queue.storage.queued = (upTo, cb) -> cb null, ['id1', 'id2']
            queue.storage.mget = (ids, cb) -> cb null, tasks
            queue.storage.requeue = (id, time, cb) -> cb null

            queuedSpy = sinon.spy queue.storage, 'queued'
            queue._runTask = sinon.spy()

            ran = Date.now()
            async.series [
                (cb) ->
                    queue._run()
                    cb null
                (cb) ->
                    setTimeout ->
                        queuedSpy.calledOnce.should.eql yes
                        queuedSpy.args[0][0].should.be.approximately(ran, 20)

                        queue._runTask.calledTwice.should.eql yes
                        cb null
                    , 200
            ], done

    describe '#start', ->
        it 'should set interval on _run function', (done) ->
            queue._run = sinon.spy()

            queue.updateInterval = 50
            queue.start()
            queue._run.calledOnce.should.eql yes
            should.exist queue._iid

            setTimeout ->
                clearInterval queue._iid
                queue._run.calledTwice.should.eql yes
                done()
            , 60

    describe '#stop', ->
        it 'should clear interval on _run function', (done) ->
            queue._run = sinon.spy()

            queue.updateInterval = 50
            queue.start()
            queue.stop()
            setTimeout ->
                queue._run.calledOnce.should.eql yes

                done()
            , 60
