should = require 'should'
async = require 'async'

RedisStorage = require '../../lib/storage/redis'

describe.skip 'RedisStorage', ->
    storage = null
    before ->
        storage = new RedisStorage 'test', modules.redis
    afterEach (done) ->
        async.parallel [
            (cb) -> modules.redis.del storage._keys.queue, cb
            (cb) -> modules.redis.del storage._keys.data, cb
        ], done

    describe 'constructor', ->
        it 'should initialize correct queue names', ->
            storage._keys.queue.should.eql 'st:queue:test'
            storage._keys.data.should.eql 'st:data:test'
        it 'should store redis client', ->
            should.exist storage.redis
    describe '#clear', ->
        it 'should remove queue and data redis items', (done) ->
            async.series [
                (cb) ->
                    modules.redis.hmset [
                        storage._keys.data
                        'id1'
                        'val1'
                        'id2'
                        'val2'
                    ], cb
                (cb) ->
                    modules.redis.zadd [
                        storage._keys.queue
                        123
                        'id1'
                        34253
                        'id2'
                    ], cb
                (cb) ->
                    storage.clear cb
                (cb) ->
                    modules.redis.type storage._keys.data, (err, type) ->
                        should.not.exist err
                        type.should.eql 'none'
                        cb null
                (cb) ->
                    modules.redis.type storage._keys.queue, (err, type) ->
                        should.not.exist err
                        type.should.eql 'none'
                        cb null
            ], done

    describe 'operations with data', ->
        describe '#set', ->
            it 'should return error if id or data is not given', (done) ->
                async.parallel [
                    (cb) ->
                        storage.set null, null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.set 'id', null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.set null, 'data', (err) ->
                            should.exist err
                            cb null
                ], done
            it 'should save given value', (done) ->
                async.parallel [
                    (cb) ->
                        key = 'id'
                        value = data: 'data'
                        async.series [
                            (cb) ->
                                storage.set key, value, cb
                            (cb) ->
                                modules.redis.hget storage._keys.data, key, (err, data) ->
                                    should.not.exist err
                                    should.exist data
                                    data.should.eql JSON.stringify value
                                    cb null
                        ], cb
                    (cb) ->
                        key = 'testId'
                        value = key: 'value'
                        async.series [
                            (cb) ->
                                storage.set key, value, cb
                            (cb) ->
                                modules.redis.hget storage._keys.data, key, (err, data) ->
                                    should.not.exist err
                                    should.exist data
                                    data.should.eql JSON.stringify value
                                    cb null
                        ], cb
                ], done
        describe '#get', ->
            it 'should return error if id is not give', (done) ->
                storage.get null, (err) ->
                    should.exist err
                    done()
            it 'should return null if value for given key does not exist', (done) ->
                storage.get 'idontexist', (err, data) ->
                    should.not.exist err
                    should.not.exist data
                    done()
            it 'should return error if value exists but is not valid json', (done) ->
                key = 'id'
                async.series [
                    (cb) ->
                        modules.redis.hset storage._keys.data, key, '{"data":', cb
                    (cb) ->
                        storage.get key, (err, data) ->
                            should.exist err
                            should.not.exist data
                            cb null
                ], done
            it 'should return parsed value if it exists', (done) ->
                id = 'id'
                originalValue =
                    k1: 'v1'
                    k2: 'v2'
                async.series [
                    (cb) ->
                        modules.redis.hset storage._keys.data, id, JSON.stringify(originalValue), cb
                    (cb) ->
                        storage.get id, (err, value) ->
                            should.not.exist err
                            value.should.eql originalValue
                            cb null
                ], done
        describe '#mget', ->
            it 'should return error if ids is not given or is not array', (done) ->
                async.series [
                    (cb) ->
                        storage.mget null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.mget 'key', (err) ->
                            should.exist err
                            cb null
                ], done
            it 'should return hash of all present values', (done) ->
                v1 = k1: 'v1'
                v2 = k2: 'v2'
                async.series [
                    (cb) ->
                        modules.redis.hmset [
                            storage._keys.data
                            'k1'
                            JSON.stringify v1
                            'k2'
                            JSON.stringify v2
                        ], cb
                    (cb) ->
                        storage.mget ['k1', 'k2', 'k3'], (err, data) ->
                            should.not.exist err
                            should.exist data

                            data.should.have.property 'k1'
                            data.k1.should.eql v1
                            data.should.have.property 'k2'
                            data.k2.should.eql v2
                            data.should.have.property 'k3', null
                            cb null
                ], done
        describe '#remove', ->
            it 'should return error if id is not given', (done) ->
                storage.remove null, (err) ->
                    should.exist err
                    done()
            it 'should remove data if it exists', (done) ->
                async.series [
                    (cb) ->
                        modules.redis.hset storage._keys.data, 'id', 'value', cb
                    (cb) ->
                        storage.remove 'id', cb
                    (cb) ->
                        modules.redis.hget storage._keys.data, 'id', (err, val) ->
                            should.not.exist err
                            should.not.exist val
                            cb null
                ], done

    describe 'operations with queue', ->
        describe '#enqueue', ->
            it 'should not add item if time and id are not set or time is not number', (cb) ->
                async.parallel [
                    (cb) ->
                        storage.enqueue null, null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.enqueue 'id', null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.enqueue null, 123, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.enqueue null, '123', (err) ->
                            should.exist err
                            cb null
                ], cb
            it 'should correctly add item', (done) ->
                async.series [
                    (cb) ->
                        id = 'id'
                        originalTime = 123
                        async.series [
                            (cb) ->
                                storage.enqueue id, originalTime, cb
                            (cb) ->
                                modules.redis.zscore storage._keys.queue, id, (err, time) ->
                                    should.not.exist err
                                    should.exist time
                                    time.should.eql originalTime
                                    cb null
                        ], cb
                    (cb) ->
                        id = 'id'
                        originalTime = 135345345
                        async.series [
                            (cb) ->
                                storage.enqueue id, originalTime, cb
                            (cb) ->
                                modules.redis.zscore storage._keys.queue, id, (err, time) ->
                                    should.not.exist err
                                    should.exist time
                                    time.should.eql originalTime
                                    cb null
                        ], cb
                    (cb) ->
                        id = 'otherid'
                        originalTime = 3243423
                        async.series [
                            (cb) ->
                                storage.enqueue id, originalTime, cb
                            (cb) ->
                                modules.redis.zscore storage._keys.queue, id, (err, time) ->
                                    should.not.exist err
                                    should.exist time
                                    time.should.eql originalTime
                                    cb null
                        ], cb
                ], done
        describe '#dequeue', ->
            it 'should remove error if id is not set', (done) ->
                storage.dequeue null, (err) ->
                    should.exist err
                    done()
            it 'should remove item if it exists', (done) ->
                key = 'id'
                async.series [
                    (cb) ->
                        modules.redis.zadd storage._keys.queue, 123, key, cb
                    (cb) ->
                        storage.dequeue key, cb
                    (cb) ->
                        modules.redis.zscore storage._keys.queue, key, (err, score) ->
                            should.not.exist err
                            should.not.exist score
                            cb null
                ], done
        describe '#requeue', ->
            it 'should return error if id and time are not given', (done) ->
                async.parallel [
                    (cb) ->
                        storage.requeue null, null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.requeue 'id', null, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.requeue null, 123, (err) ->
                            should.exist err
                            cb null
                    (cb) ->
                        storage.requeue null, '123', (err) ->
                            should.exist err
                            cb null
                ], done
            it 'should not update item if it exists and its update time is less than given time', (done) ->
                tasks = [
                    {
                        id: 'id1'
                        originalTime: 123
                        newTime: 333
                    }
                    {
                        id: 'id2'
                        originalTime: 1283172978391
                        newTime: 1283172978392
                    }
                ]
                async.each tasks, (task, cb) ->
                    async.series [
                        (cb) ->
                            modules.redis.zadd storage._keys.queue, task.originalTime, task.id, cb
                        (cb) ->
                            storage.requeue task.id, task.newTime, cb
                        (cb) ->
                            modules.redis.zscore storage._keys.queue, task.id, (err, time) ->
                                should.not.exist err
                                should.exist time
                                time.should.eql task.originalTime
                                cb null
                    ], cb
                , done

            it 'should update time if item exists and its current time is bigger than given', (done) ->
                tasks = [
                    {
                        id: 'id1'
                        originalTime: 423
                        newTime: 333
                    }
                    {
                        id: 'id2'
                        originalTime: 1283172978393
                        newTime: 1283172978392
                    }
                    {
                        id: 'id3'
                        originalTime: 0
                        newTime: 1283172978392
                    }
                ]
                async.each tasks, (task, cb) ->
                    async.series [
                        (cb) ->
                            modules.redis.zadd storage._keys.queue, task.originalTime, task.id, cb
                        (cb) ->
                            storage.requeue task.id, task.newTime, cb
                        (cb) ->
                            modules.redis.zscore storage._keys.queue, task.id, (err, time) ->
                                should.not.exist err
                                should.exist time
                                time.should.eql task.newTime
                                cb null
                    ], cb
                , done
            it 'should add item if it does not exist', (done) ->
                tasks = [
                    {
                        id: 'id1'
                        newTime: 423
                    }
                    {
                        id: 'id2'
                        newTime: 1283172978393
                    }
                ]
                async.each tasks, (task, cb) ->
                    async.series [
                        (cb) ->
                            modules.redis.zrem storage._keys.queue, task.id, cb
                        (cb) ->
                            storage.requeue task.id, task.newTime, cb
                        (cb) ->
                            modules.redis.zscore storage._keys.queue, task.id, (err, time) ->
                                should.not.exist err
                                should.exist time
                                time.should.eql task.newTime
                                cb null
                    ], cb
                , done
        describe '#queued', ->
            it 'should return all items with time less then given and set their time to 0', (done) ->
                fill = (cb) ->
                    modules.redis.zadd [
                        storage._keys.queue
                        0
                        'zero'
                        123
                        'a'
                        234
                        'b'
                        345
                        'c'
                        456
                        'd'
                    ], cb
                clear = (cb) ->
                    storage.clear cb
                async.series [
                    fill
                    (cb) ->
                        storage.queued 124, (err, items) ->
                            should.not.exist err
                            items.should.have.length 1
                            items.should.containEql 'a'
                            cb null
                    (cb) ->
                        modules.redis.zcount storage._keys.queue, 0, 0, (err, cnt) ->
                            should.not.exist err
                            cnt.should.eql 2
                            cb null
                    clear
                    fill
                    (cb) ->
                        storage.queued 234, (err, items) ->
                            should.not.exist err
                            items.should.have.length 2
                            items.should.containEql 'a'
                            items.should.containEql 'b'
                            cb null
                    (cb) ->
                        modules.redis.zcount storage._keys.queue, 0, 0, (err, cnt) ->
                            should.not.exist err
                            cnt.should.eql 3
                            cb null
                    clear
                    fill
                    (cb) ->
                        storage.queued 9001, (err, items) ->
                            should.not.exist err
                            items.should.have.length 4
                            items.should.containEql 'a'
                            items.should.containEql 'b'
                            items.should.containEql 'c'
                            items.should.containEql 'd'
                            cb null
                    (cb) ->
                        modules.redis.zcount storage._keys.queue, 0, 0, (err, cnt) ->
                            should.not.exist err
                            cnt.should.eql 5
                            cb null
                ], done
